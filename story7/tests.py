from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views
# Create your tests here.

class UnitTestForStory7(TestCase):
    def test_response_page(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7.html')

    def test_func_page(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, views.story7Views)