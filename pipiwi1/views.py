from django.http import HttpResponse
from django.shortcuts import render, redirect



def index(request):
    response = {}
    return render(request,"landing.html",response)

def resume(request):
    response = {}
    return render(request,"resume.html",response)

def story1(request):
    response = {}
    return render(request,"story1.html",response)

def story3(request):
    response = {}
    return render(request,"story3.html",response)


