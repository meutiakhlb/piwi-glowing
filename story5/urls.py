from django.urls import path
from . import views
from .views import *

app_name = 'story5'

urlpatterns = [
    path('', views.story5),
    path('post', views.post_story5),
    path('delete/<str:nama_matkul>', views.delete_jadwal),
    path('listJadwal', views.listJadwal),

]
