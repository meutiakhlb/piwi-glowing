from django import forms

class jadwalForm(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class': 'form-control'
    }
    matkul = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Mata Kuliah ', max_length=50, required=True)
    semester = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Semester / Tahun', max_length=50, required=True)
    Dosen= forms.CharField(widget=forms.TextInput(attrs=attrs), label='Nama Dosen ',max_length=50,required=True)
    Kelas = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Ruang Kelas ',max_length=50, required=True)
    sks = forms.IntegerField( widget=forms.NumberInput(attrs=attrs),label='Jumlah SKS ', required=True)
    deskripsi = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Deskripsi', max_length=50, required=True)

