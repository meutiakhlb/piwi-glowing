from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

class schedule (models.Model):
    matkul = models.CharField(max_length=120)
    semester = models.CharField(max_length=20, default=("Gasal 2019/2020"))
    Dosen = models.CharField(max_length=120)
    Kelas = models.CharField(max_length=120, default = "Online Class")
    sks = models.IntegerField()
    deskripsi= models.TextField(max_length=120,null=True, default="-")

    def __str__(self):
        return self.matkul
