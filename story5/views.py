from django.http import HttpResponseRedirect
from django.shortcuts import render,redirect
from .forms import jadwalForm
from .models import schedule

# Create your views here.
def post_story5(request):
    if request.method == 'POST':
        response_data = {}
        try:
            sks = int(request.POST['sks'])
        except ValueError:
            return redirect('/story5')

        form = jadwalForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            response_data['matkul'] = request.POST['matkul']
            response_data['semester'] = request.POST['semester']
            response_data['Dosen'] = request.POST['Dosen']
            response_data['Kelas'] = request.POST['Kelas']
            response_data['sks'] = request.POST['sks']
            response_data['deskripsi'] = request.POST['deskripsi']

            data_matkul = schedule(matkul=response_data['matkul'],
                                    semester=response_data['semester'],
                                    Dosen=response_data['Dosen'],
                                    Kelas=response_data['Kelas'],
                                    sks=response_data['sks'],
                                    deskripsi=response_data['deskripsi'],
            )
            data_matkul.save()
            return redirect('/story5/')
        else:
            return redirect('/story5')
    else:
        return redirect('/story5')



def story5(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        response_data = {}
        try:
            jumlahsks = int(request.POST['sks'])
        except ValueError:
            return redirect('/story5')

        form = jadwalForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            response_data['matkul'] = form.cleaned_data['matkul']
            response_data['semester'] = form.cleaned_data['semester']
            response_data['Dosen'] = form.cleaned_data['Dosen']
            response_data['Kelas'] = form.cleaned_data['Kelas']
            response_data['sks'] =form.cleaned_data['sks']
            response_data['deskripsi'] = form.cleaned_data['deskripsi']

            data_matkul = schedule(matkul=response_data['matkul'],
                                semester=response_data['semester'],
                                Dosen=response_data['Dosen'],
                                Kelas=response_data['Kelas'],
                                sks=response_data['sks'],
                                deskripsi=response_data['deskripsi'],
                                )
            data_matkul.save()
            return redirect('/story5/listJadwal')
        else:
            return redirect('/story5')

    form_tambah = jadwalForm()
    data = schedule.objects.all()
    response = {
        'form_tambah': form_tambah,
        'data': data,
    }

    return render(request, 'addMatkul.html', response)

def listJadwal(request):
    data = schedule.objects.all()
    response = {
        'data': data,
    }
    return render(request, 'jadwal.html', response)

def delete_jadwal(request,nama_matkul):
    data = schedule.objects.filter(matkul=nama_matkul)
    data.delete()
    return redirect('/story5')
