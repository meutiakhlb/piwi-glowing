from django.shortcuts import render, redirect
from .forms import activityForm, PesertaForm
from .models import Kegiatan, Peserta


# Create your views here.
def story6(request):
    datas = {
        'kegiatan': Kegiatan.objects.all(),
        'peserta': Peserta.objects.all(),
    }
    data_forms = {
        'kegiatan_form': activityForm(),
        'peserta_form': PesertaForm(),
    }
    response = {
        'forms': data_forms,
        'data': datas
    }
    return render(request, 'story6.html', response)


def post_kegiatan(request):
    if request.method == 'POST':
        form = activityForm(request.POST or None)
        if form.is_valid():
            data_form = form.cleaned_data
            data = Kegiatan(nama=data_form['nama_kegiatan'])
            data.save()
            return redirect('/story6/')
        else:
            return redirect('/story6/')
    else:
        return redirect('/story6/')


def post_peserta(request, id_kegiatan):
    if request.method == 'POST':
        form = PesertaForm(request.POST or None)
        if form.is_valid():
            kegiatan = Kegiatan.objects.get(id=id_kegiatan)
            data_form = form.cleaned_data
            data_input = Peserta()
            data_input.nama = data_form['nama_peserta']
            data_input.kegiatan = kegiatan

            data_input.save()
            return redirect('/story6/')
        else:
            return redirect('/story6/')
    else:
        return redirect('/story6/')

