from django.shortcuts import render
from django.http import JsonResponse
import requests as res
import json

# Create your views here.
app_name = 'story8'

def story8(request):
    return render(request, 'story8.html')

def bookAPI(request):
    q = request.GET['q']
    url = "https://www.googleapis.com/books/v1/volumes?q=" + q
    obj = res.get(url)
    data = json.loads(obj.content)
    return JsonResponse(data, safe = False)

